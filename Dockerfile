FROM ubuntu:22.04

RUN apt update && apt install -y --no-install-recommends python3 python3-pip python3-venv

# Docker-in-docker
RUN apt update && apt install -y \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
RUN mkdir -m 0755 -p /etc/apt/keyrings
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt update && apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
RUN apt clean && rm -rf /var/lib/apt/lists/*


COPY requirements.txt /opt/eva-discord-bot/requirements.txt
WORKDIR /opt/eva-discord-bot/

RUN python3 -m venv venv
SHELL ["/bin/bash", "-c"]
RUN source venv/bin/activate && pip3 install -r requirements.txt
SHELL ["/bin/sh", "-c"]

COPY ./ /opt/eva-discord-bot/

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
