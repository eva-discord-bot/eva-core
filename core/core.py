
import os
import re
import hikari
import traceback

from loguru import logger

from .module_wrap import ModuleWrap
from .module_loader import ModuleLoader
from .subscription import SubChannelWide, SubChannelSpecific
from .eva_message import EvaMessage, EvaMessageBuilder
from .constants import *

class Core:
    def __init__(self):
        self.module_wrap_list = list()
        self.subscriber_list = list()

        self.module_loader = ModuleLoader(self)

        self.crash_img = hikari.File("crashed-robot.jpg")

    def startup(self):
        self._print_logo()
        self.load_modules()

        for mw in self.module_wrap_list:
            self.add_subscriber(mw)

    def load_modules(self):
        available_modules = self.module_loader.search_modules()

        if available_modules:
            for module_name in available_modules:
                config = self.module_loader.check_module(module_name)
                if not config:
                    continue

                module_obj = self.module_loader.load_module(module_name, config)
                if not module_obj:
                    continue

                module_wrap = ModuleWrap(config, module_obj)
                self.module_wrap_list.append(module_wrap)

    def add_subscriber(self, module_wrap):
        if module_wrap.config["module"]["channel_wide"]:
            self.subscriber_list.append(SubChannelWide(module_wrap))
        else:
            for ch in module_wrap.config["module"]["allowed_channels"]:
                self.subscriber_list.append(SubChannelSpecific(module_wrap, ch))

    async def event_msg(self, event):
        #print("incomming msg")
        channel = event.get_channel().name
        content = event.content.lower()
        words = re.findall(r'\S+', content)
        #print(event)

        # channel filter
        ch_wide_l = list(filter(lambda sub: isinstance(sub, SubChannelWide), self.subscriber_list))
        ch_specific_l = list(filter(lambda sub: isinstance(sub, SubChannelSpecific) and sub.channel == channel, self.subscriber_list))
        filter_1 = list()
        filter_1.extend(ch_specific_l)
        filter_1.extend(ch_wide_l)

        # prefix filter and apply
        for sub in filter_1:
            word_index = 0

            prefix_bot_name_ok = False
            prefix_module_name_ok = False

            message_builder = EvaMessageBuilder(self)
            message_builder.set_hikari_message(event.message)

            # prefix filter
            if word_index < len(words) and words[word_index] == "eva":
                message_builder.set_prefix_bot_name()
                word_index += 1
                prefix_bot_name_ok = True
            if word_index < len(words) and words[word_index] == sub.get_module_name():
                message_builder.set_prefix_module_name(words[word_index])
                word_index += 1
                prefix_module_name_ok = True

            if sub.prefix_bot_name and not prefix_bot_name_ok: continue
            if sub.prefix_module_name and not prefix_module_name_ok: continue

            # apply
            message_builder.set_words(words[word_index:])

            try:
                await sub.module_wrap.module_obj.event_msg(message_builder.build())
            except:
                traceback.print_exc()
                await event.message.respond("", attachment=self.crash_img)

    async def send_msg(self, hikari_message, message, attachment = None):
        await hikari_message.respond(message, attachment = attachment)

    def _print_logo(self):
        if not os.path.exists(file_logo_ascii):
            logger.warning(f"Cannot locate EVA ascii art logo.")
            return

        with open(file_logo_ascii, 'r') as file:
            for line in file:
                logger.info(line.rstrip())
