
# Type checking
import hikari.messages
from typing import TYPE_CHECKING, List
if TYPE_CHECKING:
    from .core import Core


class EvaMessage:
    def __init__(self,
            core_obj: "Core",
            hikari_message: hikari.messages,
            words: List[str],
            prefix_bot_name: bool,
            prefix_module_name: str
    ) -> None:
        self._core_obj = core_obj
        self._hikari_message = hikari_message
        self.words = words
        self.prefix_bot_name = prefix_bot_name
        self.prefix_module_name = prefix_module_name

    async def respond(self, message: str, attachment = None):
        await self._core_obj.send_msg(self._hikari_message, message, attachment)

class EvaMessageBuilder:
    def __init__(self, core_obj: "Core") -> None:
        self._core_obj = core_obj
        self._hikari_message = None
        self.prefix_bot_name = False
        self.prefix_module_name = ""
        self.words = list()

    def set_hikari_message(self, hikari_message: hikari.messages) -> None:
        self._hikari_message = hikari_message

    def set_words(self, words: List[str]) -> None:
        self.words = words

    def set_prefix_bot_name(self) -> None:
        self.prefix_bot_name = True

    def set_prefix_module_name(self, prefix_module_name: str) -> None:
        self.prefix_module_name = prefix_module_name

    def build(self) -> EvaMessage:
        return EvaMessage(self._core_obj, self._hikari_message, self.words, self.prefix_bot_name, self.prefix_module_name)
