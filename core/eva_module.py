
from loguru import logger
from abc import ABC, abstractmethod

class EvaModule(ABC):
    @abstractmethod
    async def event_msg(self, eva_message):
        raise NotImplementedError("Must override method \"event_msg(message, words)\"")
