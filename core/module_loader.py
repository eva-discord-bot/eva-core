
import os
import yaml
import jsonschema
import importlib
import traceback

from loguru import logger

from .constants import *

class ModuleLoader:
    def __init__(self, core):
        self.core = core

    def search_modules(self):
        # check "modules" dir
        if not os.path.exists(dir_modules):
            logger.error(f"Directory for modules \"{dir_modules}\" does not exists. There is no module to load.")
            return False

        walk = os.walk(dir_modules)
        module_list = next(walk)[1]

        if not module_list:
            logger.error(f"Directory with modules is empty. There is no module to load.")
            return False

        logger.info(f"Available modules: {module_list}")
        return module_list

    def check_module(self, module_name):
        logger.info(f"Checking module \"{module_name}\"...")
        path_module = f"{dir_modules}{module_name}/"

        config = self._config_load(path_module)
        if not config or not self._config_validate(path_module, config):
            logger.error(f"Checking module \"{module_name}\" failed")
            return False

        logger.info(f"Checking module \"{module_name}\" done.")
        return config

    def load_module(self, module_name, config):
        try:
            logger.info(f"Loading module \"{module_name}\"...")
            module = importlib.import_module(f"modules.{config['module']['name']}")
            moduleClassName = getattr(module, config['module']['class_name'])
            logger.info(f"Loading module \"{module_name}\" done.")
        except:
            logger.error(f"Loading module \"{module_name}\" failed.")
            traceback.print_exc()
            return None

        try:
            logger.info(f"Initializing module \"{module_name}\"...")
            module_obj = moduleClassName()
            logger.info(f"Initializing module \"{module_name}\" done.")
        except:
            logger.error(f"Initializing module \"{module_name}\" failed.")
            traceback.print_exc()
            return None

        return module_obj

    def _config_load(self, path_config):
        path_config_full = f"{path_config}/{file_config_modules}"

        if not os.path.exists(path_config_full):
            logger.error(f"config.yaml file is missing for module \"{path_config}\".")
            return False

        with open(path_config_full, 'r') as file:
            config = yaml.safe_load(file)
        return config

    def _config_validate(self, path_module, config):
        with open(file_config_modules_schema, 'r') as file:
            schema = yaml.safe_load(file)

        try:
            jsonschema.validate(config, schema)
            return True
        except jsonschema.exceptions.ValidationError:
            logger.error(f"Module located in \"{path_module}\" does not have valid configuration file.")
            return False
