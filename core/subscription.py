
from abc import ABC, abstractmethod

class Subscription(ABC):
    def __init__(self, module_wrap):
        self.module_wrap = module_wrap

        config = module_wrap.config

        self.prefix_bot_name = config["module"]["force_prefix_activation"]["bot_name"]
        self.prefix_module_name = config["module"]["force_prefix_activation"]["module_name"]

    def get_module_name(self):
        return self.module_wrap.module_name

class SubChannelWide(Subscription):
    pass

class SubChannelSpecific(Subscription):
    def __init__(self, module_wrap, channel):
        super().__init__(module_wrap)
        self.channel = channel
