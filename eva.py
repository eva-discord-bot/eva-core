#!venv/bin/python3

import os
import hikari
import core


intents = hikari.Intents.ALL_UNPRIVILEGED | hikari.Intents.MESSAGE_CONTENT
eva = hikari.GatewayBot(token=os.environ["EVA_TOKEN"], intents=intents)

eva_core = core.Core()
eva_core.startup()


@eva.listen()
async def msg_create_event(event: hikari.GuildMessageCreateEvent) -> None:
    if event.is_bot or not event.content:
        return

    await eva_core.event_msg(event)

@eva.listen()
async def msg_create_event(event: hikari.DMMessageCreateEvent) -> None:
    if event.is_bot or not event.content:
        return

    await eva_core.event_msg(event)

eva.run()
