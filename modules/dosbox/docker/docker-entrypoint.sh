#!/bin/bash

set -xe

USER=dosboxuser
USER_HOME=/home/${USER}

echo $USE_DISPLAY
export WEBPORT=590${USE_DISPLAY}
export DISPLAY=:${USE_DISPLAY}

echo $WEBPORT
echo $DISPLAY

mkdir $USER_HOME/.vnc
echo $VNC_PASS
echo $VNC_PASS | vncpasswd -f > $USER_HOME/.vnc/passwd
chmod 0600 $USER_HOME/.vnc/passwd

/opt/websockify/run ${WEBPORT} --verbose --web=/opt/noVNC --wrap-mode=ignore -- vncserver ${DISPLAY} -geometry 640x400 -listen TCP -xstartup /tmp/xsession
