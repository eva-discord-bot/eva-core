
import subprocess
import docker
import os
import csv

from loguru import logger

from core import EvaModule, EvaMessage


class EvaModuleDosbox(EvaModule):
    def __init__(self):
        self.game_list = list()
        with open("modules/dosbox/game_list.csv") as f:
            game_list_file = csv.DictReader(f)
            for entry in game_list_file:
                self.game_list.append(entry)

        self.image = os.environ["EVA_MODULE_DOSBOX_IMAGE"]
        self.host = os.environ["EVA_MODULE_DOSBOX_HOST"]
        self.vnc_pass = os.environ["EVA_MODULE_DOSBOX_VNC_PASS"]
        self.game_vol_path = os.environ["EVA_MODULE_DOSBOX_GAME_PATH"]
        self.id_min = 1
        self.id_max = 5

        self._docker = docker.from_env()

        logger.info("Pulling the dosbox docker image...")
        self._docker.images.pull(self.image)

    def _get_game_path(self, name: str) -> dict:
        for entry in self.game_list:
            if entry["Name"] == name:
                return entry["Path"]

        return None

    def _get_container(self, id: int):
        containers = self._docker.containers.list()

        if not containers:
            return None

        for cont in containers:
            cont_words = cont.name.split('-')

            # skip wrong container name
            if len(cont_words) < 2 or cont_words[0] != "dosbox":
                continue

            if cont_words[1] == str(id):
                return cont

        return None

    def _collect(self, col: list):
        col.sort()
        result = "\n".join(col)
        return f"```\n{result}\n```"

    def _list_games(self):
        if not self.game_list:
            return "Není nainstalovaná žádná hra."

        result = list()
        for entry in self.game_list:
            result.append(f"{entry['Name']}")
        return self._collect(result)

    def _list_running(self):
        cont_all = self._docker.containers.list()
        cont_names = list()
        for c in cont_all:
            if c.name.startswith("dosbox"):
                cont_names.append(c.name)

        if not cont_names:
            return "Není spuštěna žádná hra."
        return self._collect(cont_names)

    def _start(self, game: str):
        # try to find unused ID
        id = -1
        for candidate in range(self.id_min, self.id_max+1):
            if not self._get_container(candidate):
                id = candidate

        if id == -1:
            return f"V jednom okamžiku může běžet maximálně {self.id_max - self.id_min + 1} her a žádné volné ID už nezbylo. Zkus nějakou hru ukončit příkazem `dosbox stop ID`. Aktuálně běží tohle: \n" + self._list_running()

        id_str = f"dosbox-{str(id)}"

        game_path = self._get_game_path(game)
        if not game_path:
            return f"Hra se jménem \"{game}\" nebyla nalezena."

        container = self._docker.containers.run(
            self.image,
            name=id_str,
            remove=True,
            detach=True,
            privileged=False,
            user=1000,
            ports={
                f"590{str(id)}/tcp": (5900 + id)
            },
            environment = {
                "USE_DISPLAY": str(id),
                "VNC_PASS": self.vnc_pass,
                "GAME": game_path
            },
            volumes = [
                f"{self.game_vol_path}:/games"
            ]
        )

        return f"http://{self.host}:590{id}?resize=scale&password={self.vnc_pass}"

    def _stop(self, id_str: str):
        try:
            id = int(id_str)
        except ValueError:
            return "Zadané ID není platné."

        cont = self._get_container(id)
        if not cont:
            return "Pod zadaným ID není aktuálně spustěna žádná hra."
        cont.stop()
        return f"Hra s ID {id_str} byla ukončena."

    def _stop_all(self):
        for id in range(self.id_min, self.id_max+1):
            self._stop(id)

        return "Všechny běžící hry byly ukončeny."

    def _parser(self, words):
        if   words[0] ==  "running": return self._list_running()
        elif words[0] ==    "games": return self._list_games()
        elif words[0] == "stop-all": return self._stop_all()

        if len(words) < 2:
            return # TODO eva unknown command

        if   words[0] == "start": return self._start(words[1])
        elif words[0] ==  "stop": return self._stop(words[1])
        else: return # TODO eva unknown command

    async def event_msg(self, eva_message: EvaMessage):
        await eva_message.respond(self._parser(eva_message.words))
