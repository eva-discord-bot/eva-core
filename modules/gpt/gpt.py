
import os
import openai

from core import EvaModule, EvaMessage

class EvaModuleGpt(EvaModule):
    def __init__(self):
        openai.api_key = os.environ["EVA_MODULE_GPT_TOKEN"]

        self.model_engine = "text-davinci-003"
        #self.model_engine = "code-davinci-002"

    async def event_msg(self, eva_message: EvaMessage):
        prompt = str.join(" ", eva_message.words)

        completions = openai.Completion.create(
            engine=self.model_engine,
            prompt=prompt,
            max_tokens=3072,
            n=1,
            stop=None,
            temperature=0.5,
        )

        generated_text = completions.choices[0].text
        await eva_message.respond(generated_text)
