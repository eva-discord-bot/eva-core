
from core import EvaModule, EvaMessage

class EvaModuleHelp(EvaModule):
    async def event_msg(self, eva_message: EvaMessage):
        if eva_message.prefix_module_name == "help":
            if len(eva_message.words) == 0:
                with open("modules/help/help.txt", "r") as file:
                    await eva_message.respond(f"```\n{file.read()}\n```")
            else:
                await eva_message.respond("Not implemented")
        elif eva_message.prefix_bot_name \
                and len(eva_message.words) > 0 \
                and eva_message.words[0] == "modules":
            await eva_message.respond("Not implemented")
