
from core import EvaModule, EvaMessage

class EvaModulePing(EvaModule):
    async def event_msg(self, eva_message: EvaMessage):
        await eva_message.respond("Pong!")
