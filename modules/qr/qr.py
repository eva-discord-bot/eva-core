
import hikari
import random
import qrcode
from datetime import date

from core import EvaModule, EvaMessage

class EvaModuleQr(EvaModule):
    async def event_msg(self, eva_message: EvaMessage):
        qr = qrcode.QRCode(version=1, box_size=10, border=5)
        qr.add_data(str.join(" ", eva_message.words))
        qr.make(fit=True)

        img = qr.make_image(fill='black', back_color='white')

        today = date.today()
        today_str = today.strftime("%b-%d-%Y")
        random_num = str(random.randrange(1000, 10000))
        filename = f'{today_str}-{random_num}.png'
        img.save(filename)

        qr_code = hikari.File(filename)
        await eva_message.respond("", attachment=qr_code)
